
//-----------add checkFastqFinished() 2017.2.9-----------
//---------add QC interface and writeFq 2016.8.9---------
//----add enum QCvalueType; dict fovQCInfo 2016.11.24----
#include "Ice/Identity.ice"

module CGI {
    sequence<string> StringVector;
    sequence<int> IntVector;
    sequence<float> FloatVector;
    sequence<FloatVector> FloatMatrix;
    sequence<double> DoubleVector;

    module OSIIP {

        /* Constants */
        const string ProcessorId = "Processor";
        
        enum FPState
        {
            Running,
            Stopping,
            Stopped,
            ShutingDown
        };

        //["cs:attribute:System.Serializable"]
        struct BatchId
        {
			int cycle;
			int lane;
            string slide;
        };

		struct SyncBatchId
        {
			int lane;
            string slide;
        };


        enum ImageDye
        {
            //A:AF532 instead of CY3; 
            //C: IF700; G: CY5; T: ROX;
            
			CY3, IF700, CY5, ROX, AF532
            
			//2-color: A-AF532+CY5 T-AF532 C-CY5 G-nolable
        };

		enum ImageChannel
        {
			Channel0,
			Channel1,
			Channel2,
			Channel3
        };
		
       enum ChannelType
		{
           Abase,
           Cbase,
           Gbase,
           Tbase
		};

		struct FOVLocation
		{
			int col;
			int row;
		};
		
		struct Point
		{
			float x;
			float y;
		};
		
		struct DyeF
        {
            float fitc;
            float cy3;
            float txrd;
            float cy5;
        };
		
		struct BaseF
		{
			float A;
			float C;
			float T;
			float G;
		};
		
		struct DyePt
		{
			Point fitc;
			Point cy3;
			Point txrd;
			Point cy5;
		};
		dictionary<FOVLocation, int> FOVLocationDict;
		//------------------------
        enum QCvalueType
		{
           IMAGEnum,		    
           sigA, sigC, sigG, sigT, sigH, sigL,
		   bkgA, bkgC, bkgG, bkgT, bkgH, bkgL,
		   movAx,movAy, movCx,movCy, movGx,movGy, movTx,movTy, movHx,movHy, movLx,movLy,
		   
		   DNBnum,
           snrA, snrC, snrG, snrT, snrH, snrL,
		   rhoA, rhoC, rhoG, rhoT, rhoAh,rhoAl, rhoCh,rhoCl, rhoGh, rhoGl, rhoTh, rhoTl,
		   lagA, lagC, lagG, lagT, lagH, lagL,
		   ronA, ronC, ronG, ronT, ronH, ronL,
		   bic,
		   fit,
		   esr,
		   numA,numC,numG,numT
		};
		struct QCInfoTag
		{
			int cycle;
			QCvalueType valueType;
		};
		dictionary<QCInfoTag,  float> fovQC4cyc;  // QC information of a fov in 1 cycle		
		dictionary<FOVLocation, fovQC4cyc> LaneQC4cyc;
		//-----------string instead of enum::QCvalueType---------
		struct QCInfoName
		{
			int cycle;
			string valueName;
		};
		dictionary<QCInfoName, float> fovQC;
		dictionary<string, fovQC>     LaneQC;
		dictionary<FOVLocation, fovQC> LaneQCLocateName;
				
		//-------------------------------------------------------
        dictionary<ChannelType,short> ChannelDyeMap;
		
		dictionary<string, string> DCMap;
		sequence<DCMap> DCMapVector;

		dictionary<string, string> ImageAttributes;

        struct ImageMetadata
        {
            BatchId batch;
            int fovRow;
			int fovCol;
			ImageChannel ChannelInfo;
            ImageDye DyeInfo; 
			//ChannelType chanInfo;
            ImageAttributes attributes;
        };
		
		enum LaneReturnStatus
		{
			LaneSuccess,
			LackFOVforLane,
			conflictFOV
		};
		enum FovReturnStatus
		{
			FovSuccess,			
			NotFoundFOV,
			LackQCvalueForFOV,

			//processing image failed
			NotFoundTrack,
			
			//correcting Ints failed
			BadInts, //sample Failed
			
			//IO failed
			NoIntsToWrite,
			NoCalsToWrite,
			NoIntsToRead,
			NoCalsToRead			
		};

		struct QCInfoByFov
		{
			int CalledCycle; 
			float ESR;
			float bit;
			float fit;
			//float SNR[4];
			//sequence<float> SNR;
			
			//float RHO[4];
			//sequence<float> RHO;
			
			//float lag[4];
			//sequence<float> lag;
			
			//float runon[4];
			//sequence<float> runon;
			
			int CurrentCycle;
			//float signal[4];
			//sequence<float> signal;			
			
			//float backgd[4];
			DyeF backg;
			//sequence<float> backgd;
			
			//float offset[8];
			//sequence<float> offset;
		
		};
		
        sequence<short> ImageData;
        sequence<byte> DNBsCall;
        sequence<byte> DNBsScore;        
		
        exception InvalidBatchSessionState {};

        interface BatchSession {
            idempotent void renewSession();

            void submitImage(ImageMetadata metadata, ImageData data) throws InvalidBatchSessionState; 

            void imagingComplete(out bool processingComplete, out bool waiting2WriteResults);			
			
            void destroy() throws InvalidBatchSessionState;
			
			//client get QC interface
			//FovReturnStatus getQCbyFOV(FOVLocation fovL, out QCInfoByFov QCInfo);  //old 

			//use string to name fov and QCvalue, maybe use fovQC4cyc,LaneQC4cyc,LaneQCLocateName in next version
			//fovQC getFovQC(FOVLocation fovL);  //return fovQC
			FovReturnStatus getFovQC(FOVLocation fovL, out fovQC fovQCInfo);
			FovReturnStatus getFovQCByName(string fovName,   out fovQC fovQCInfo);
			
			LaneReturnStatus getLaneQC(out LaneQC LaneQCInfo);
			LaneReturnStatus getLaneQCAndDC(out LaneQC LaneQCInfo, out DCMapVector dcmapVec);

			LaneReturnStatus getLaneQCByList(FOVLocationDict fovList, out LaneQC LaneQCInfo);            
            
			//fovQC4cyc getFovQC4cyc(FOVLocation fovL);
			//LaneQC4cyc getLaneQC4cyc();
        };

        exception BatchAlreadyExists {};
        exception ActiveBatchExists {};
        exception BatchNotAccepted {};

        dictionary<string, string> ModuleParametersDict;
		dictionary<string, string> SpeciesBarcodesDict;
        dictionary<int, int> LaneMemoryDict;
		
		enum BarcodePosition
        {
            BarcodePosStart = 1,
            BarcodePosMiddle,
			BarcodePosEnd
        };

		sequence<ImageChannel> ThumbChannelsVector;
		struct ThumbnailInfo
		{
			int colMaxNum;
			int rawMaxNum;
			int ImageWidth;
			int ImageHeight;
			int sampleStep;
			ThumbChannelsVector tbChannels;
		};

		struct LocalInfo
		{
			string uploadPath;
			string imagePath;
			bool syncCals;
			bool syncImages;
			bool syncLogs;
		};
		
		struct RemoteInfo
		{
			string sshKeyPath;
			string ip;
			int port;
			string username;
		};

		enum SyncMode
        {
            NotSync,
            LocalMode,
			RemoteMode
        };

		struct SyncInfo
		{
			// mode:NotSync    do not sync the cal files, skip other parameters
			// mode:LocalMode  map storage to local, using parameters "LocalInfo"
			// mode:RemoteMode socket connection, using parameters "LocalInfo" and "RemoteInfo"
			SyncMode mode;

			// common parameters, using for mode 1 and mode 2
			LocalInfo localInfo;
			
			// just using for mode 2
			RemoteInfo remoteInfo;
		};

        class BatchParameters
        {
			int requiredMemoryMB;
			int read1Length;
			int read2Length;
			int barcodeLength;
            string sequencerName;
            string sinkLocationBase;
            ModuleParametersDict moduleParameters;			
            ChannelDyeMap channelDyeMap;
			FOVLocationDict fov2Process;
			DCMapVector dcMapVec;
			bool calculateDC;
			BarcodePosition barcodePos;
			optional(5) int imgResolutionX;
			optional(6) int imgResolutionY;
	
			bool saveThumbnail;//if true, then should set thumbnailinfo
			ThumbnailInfo thumbnail;

			optional(1) SyncInfo syncInfo;
        };
		
        enum ReserveStatus
        {
            ReserveAccepted,
            ReserveFailed
        };

        struct ReserveResult
        {
            ReserveStatus status;
            string msg;
        };

        struct ReserveParams
        {
            string sequencerName;
            int slide;
            int row;
			int col;
            LaneMemoryDict reserveLaneMemory;
        };

        struct ProcessorStatus
        {
            // Space available, in megabytes. When allocating a new field,
            // the client has to take into account space for all four images and
            // also leave some memory for the results of the processing and
            // intermediate objects. For 2MB images assuming 10MB space needed
            // is probably a good first approximation.
            int spaceAvailable;

            // Number of images accepted per second over the last N seconds
            int imagesAcceptedPerSec;

            // Number of images freed per second over the last N seconds
            int imagesFreedPerSec;

            // Number of tasks in the cpu task engine queue
            int cpuQueueDepth;

            // Number of tasks in the IO task engine queue
            int ioQueueDepth;

            // Number of batches currently in progress
            int numBatchesInProgress;

            // Name of the last batch started or opened
            string lastBatchName;

            // Name of the slide-cycler that submitted the last batch
            string lastSlideCycler;

            // Time the last batch was started or opened
            string lastStartTime;

            // The processor software version
            string processorVersion;

            // 1 = the sink file writer is not making progress, 0 = sink file writer ok
            int sinkFileWriterHanging;
            
            // Number of batches currently in progress that are in Imaging State
            int numActiveImagingBatchesInProgress;
            
            // Full list of Imager/Batch opened
            string imagersBatches;

            // Number of reserved batches currently 
            int numReservedBatches;
            
            // Reserved Memory, in MB
            int spaceReserved;
            
            // FP state
            FPState nodeState;
        };

        struct ProcessorPerfStats
        {
            // Number of the CPUs on the machine (as reported by the OS)
            int cpuCount;

            // 15-minute load average, as reported by the OS
            float loadAverage15;

            // Same as in processor status struct.
            int spaceAvailableMb;

            // Exponential moving average of the space consumption rate, computed
            // as if the storage was down (that is, space freed as a result of
            // flushing sink buffers is counted as not freed).
            float spaceConsumptionRateMbPerSec;
        };

		enum EndCycProcessMode //not used
		{
			R1R2None, 
			R1Only,
			R2Only,
			R1R2Both

		};

		struct ExperimentInfo
		{
			string key;
			string value;
		};
		sequence<ExperimentInfo> ExperimentInfoVector;
		class LaneParams
		{
			int useDefaultSettings; //default 0, when =1 then just valid read1Length,read2Length,speciesBarcodes
			int sequencerType; //seq-500,seq-50	
			int filterMode;    //=0 no filter; =1 for seq-500 normal filter rules; =2 for seq-50			
			int lane;
			int TotalCycle;	
			
			//BatchParameters parametersForFq;
			int fovMaxC;
			int fovMaxR;
			int read1Length;
			int read2Length;
			int barcodeLength;
			int barcodeStartPos;
			BarcodePosition barcodePos;
			int WriteFastqMode; //default 0=fovC*fovR fovs; 1=signal fov; 2=fovList fovs;
			
			bool CalFileFlag;    //default 1=remain calfile after writeFq, =0 to delete
			int SpeciesMismatch;//default 1, for split barcode
			
			string FqPath;      //default NULL=batch params path.
			string slide;  

			FOVLocationDict fov2Process;
			SpeciesBarcodesDict speciesBarcodes;  //if NULL then do not split barcode
			
			EndCycProcessMode endCycleProcessMode;//default: false
			ExperimentInfoVector experimentInfoVec;

			optional(2) string workspacePath;
			optional(3) SyncInfo syncInfo;
		};
		//get qc file path
        sequence<string> FilesPath;
		dictionary<string,FilesPath> outFilesDict; //key is barcode value /failedMap /conflict /read ,value is fastq file path collection
		enum OutFileType //not used
		{
			undecoded, //also for nobarcode
			barcode1,  //barcode2 - N
			conflict,  //usaly there is no confict file
		};
        // Value returned by Processor::getInterfaceVersion
        const int processorInterfaceVersion = 100;

		interface Client
		{
		    void NotifyFov4ImagesDone(BatchId batch, string rowOfFov, string colOfFov);
		    void NotifyBatchDone(BatchId batch);
		};


		//----add for V8
		//exception BGIRemoteException 
		//{
			//string message;
		//};
		dictionary<string, int>     fovListBubbleDetectResult;

		enum IMGProcessType
		{
			BASECALLGROUPIMG,
			BUBBLEDETECTGROUPIMG,
			OTHERIMG
		};

	enum DownLoadResult
		{
			None				=0,
			CameraOneLostOne	=2,
			CameraOneLostTwo    =4,
			CameraTwoLostOne	=8,
			CameraTwoLostTwo	=16,
			Completed			=256,
		};
		
		enum CameraType
		{
			SimulatorCamera = 0,
			LumeneraCamera = 1,
			CoaxlinkGrabber = 2,
			SaperaLTCameraLink = 3,
            Andor = 4,
            PcoEdge = 5,
            PcoEdgeHyBrid = 6,
			EmergentCamera = 7,
			CoaxEuresysViewworks =8,
			ChangGuangCamera = 9
		};

		enum ScanDir
        {
            Forward,
            Reverse,
            LineIn
        };

		enum CameraMode
		{
			FreeRunCamera = 0,
			HardwareTriggerCamera,
			SoftwareTriggerCamera
		};

		struct CameraInfo
		{
			int cameraIndex;
			CameraType cameraType;

		};
		//sequence<ImageChannel> CameraChannels;

	//channelInfo define:
	//order(4):A(3):C(2):G(1):T(0)
	//01100=0x0c : AC
	//11100=0x1c : CA
	//00011=0x03 : GT
	//10011=0x13 : TG

	//01010= 0xA: AG
	//10101= 0x15: TC
		enum CameraChannels
		{
			ChannelAC = 0x0c,
			ChannelGT = 0x03,
			ChannelAG = 0x0a,
			ChannelTC = 0x15,
			ChannelA = 0x08,
			ChannelC = 0x04,
			ChannelG = 0x02,
			ChannelT = 0x01,
			ChannelACGT = 0x0f
		};


		struct CameraSetting
		{
			CameraInfo cameraInfo;
			//int cameraIndex;
			int bitsPerPixel;
			float pixelSize;
			int xResolution;
			int yResolution;
			int xOffset;
			int yOffset;
			int exposureTime;
			float gain;
			float gamma;
			//CameraType cameraType;
			int exposureNum;
			bool horizFlip;
			bool vertFlip;
			float blackLevel;
			bool useTDI;
            		ScanDir scanDirection;
            		float triggerRescalerRate;

			//for camera link only
			string serverName;
			int resourceIndex;
			string configFilePath;
			CameraMode cameraMode;
			CameraChannels cameraChannelInfo;
		};

		sequence<CameraInfo> CameraInfos;
		sequence<CameraSetting> CameraSettings;

		//struct CycleInformation
		//{
			//int imagesPerFOV;
			//string slideId;
			//int cycle;
			//int imageBitPerPixel;
			//int imageWidth;
			//int imageHeight;
			//string scanType;
			//TODO add more field
		//};

		["cs:attribute:System.Flags"]
		enum ImageTransferType
		{
            Discard              =0,
			BaseCall			 =1,
			SaveToDisk			 =2,
			CoarseTheta			 =4,
			TransferImage		 =8,
			RegField			 =16,
			ImgMetrics			 =32,
			Reserved             = 1024
		};

		enum ImageTransferColor
		{			
			FourColor1, 
			FourColor2, 
			FourColor3, 
			FourColor4, 
			TwoColor1, 
			TwoColor2
		};

		//struct ImageInformation
		//{
			//ImageTransferType imgType;
			//ImageTransferColor imgColor;
			//int cameraIndex;
			//bool isReceived;
			//int index;
		//};

		//["clr:generic:List"]
		//sequence<ImageInformation> ImageInformationList;

		struct ImageInformation
        {
            ImageTransferType imgType;
            //ImageTransferColor imgColor;
			ImageChannel imgColor;
            int cameraIndex;
            bool isReceived;
            int index;
        };

        ["clr:generic:List"]
        sequence<ImageInformation> ImageList;

		//struct FOVInformation
		//{
			//int lane;
			//int row;
			//int col;
			//int fovTypeId;  
			//Point3D targetCoord;  
			//Point3D actualCoord;
			//TODO add more field
			//ImageInformationList images;
		//};
		//["clr:generic:List"]
		//sequence<FOVInformation> FOVInformationList;

		 struct FOVInformation
        {
            int row;
            int col;
            int fovTypeId;
            int fovQuadrantId;  
            //Point3D targetCoord;  
            //Point3D actualCoord;
            int imagesPerFOV;       // image count per fov
            ImageList images;

            //TODO add more field
        
        };

        ["clr:generic:List"]
        sequence<FOVInformation> FovList;

		class CycleInfo
        {
            string slideId;
            int lane;
            int cycle;
            int rowNums;
            int colNums;
            string scanType;
            int imageBitPerPixel;	//bitPerPixel of saving tif image
            int fovTimeoutMs;

			int read1Length;
			int read2Length;
			int barcodeLength;
			BarcodePosition barcodePos;

			DCMapVector dcMapVec;//for DistortCalibration
			bool calculateDC;
			bool saveThumbnail;

			string simulateCameraPath; // for offline simulate images

			optional(4) SyncInfo syncInfo;
        };

		struct RegResults
        {
            int regStatus;			// 0-good, 1-can't cal Center, 2-CoarseTheta result is bad, 3-can't cal del, 4-del result is bad
            float pitch;			// um
            float rotation;			// rad
            float residRotation;
            int npoint;
            float horizOffset;		// pixels
            float vertOffset;		// pixels
            int rowID;
            int colID;
            float horizDelSNR;
            float vertDelSNR;
        };

		//----add for V8


        interface Processor
        {
            idempotent int getInterfaceVersion();

            ProcessorStatus getStatus();

            ReserveResult reserve(ReserveParams params);
            void cancelReserve(ReserveParams params);

            BatchSession* openBatch(BatchId batch, BatchParameters params)
                                throws BatchAlreadyExists, ActiveBatchExists, BatchNotAccepted;
            idempotent BatchSession* findBatch(BatchId batch);

            idempotent void cancelBatch(BatchId batch);

            idempotent StringVector getActiveModules();
            idempotent IntVector getSinksID();

            idempotent ProcessorPerfStats getPerfStats();
            
            void setMode(string senderHostName, string command);
            
            void submitImage(string filePath, string sinkPath);
			
			//just test Image transport
			int SaveImage(ImageMetadata metadata, ImageData data); 
			
			//writeFastq
			int generateFastQ(LaneParams laneInfo); 
			//check wether writefq finished and get fastq file path
			int checkFastqFinished(BatchId batch,out outFilesDict filesList); //batch.cycle=TotalCycle,retrun -1:not finished 1:SE 2:PE 3:SE+bar 4:PE+bar
        
		};

		//----add for V8
		
		struct ImageMetricsResults
		{
			int StatusResult;  
			DoubleVector scoreResult;
			DoubleVector SigmaResult;
			DoubleVector SNR;
		};

		interface ProcessorCallback
		{
			void AddClient(Ice::Identity ident);
			//return download result
			void DownLoadComplete(DownLoadResult downLoadResult);

			 //return image data to client for engineering UI 
            void SendImage(ImageData imgData, ImageMetadata OneImageInfor);

            //Return the Registration Results
            void SendCoarseThetaResult(FOVInformation fovInfo, RegResults regResult);

			void SendHeartbeat(int count);

			void SendImageMetrics(FOVInformation fovInfo, ImageMetricsResults imgMetrics);
			void SendBubbleDetectResult(fovListBubbleDetectResult bubbleDetectResultList);
		};
		//----add for V8


		//----add for V8 test
	enum ProState
	{
		INACTIVE = 0,
		ACTIVE,
		ACQUIREWAIT,
		ACQUIRING,
		ACQUIRECOMPLETE,
		PROCESSINGCOMPLETE,
		DEAD
	};
	interface HybridProcessor
	{
		bool SetCameraConfig(CameraSettings cameraSettings);
						//throws BGIRemoteException;

        bool CalibrateDarkFrame(string slide, int cycleNum, int picNum, bool saveToDisk);
                    //throws BGIRemoteException;
            
        bool UseDarkFrameCalibration(bool use);
                    //throws BGIRemoteException;

		bool StartCycle(CycleInfo cycleInfo);
		bool GetCycleQC(out LaneQC laneQCInfo);
        bool EndCycle();
        bool StartAcquire(FovList fovList, IMGProcessType IMGProType, bool secScan);	
        bool CompleteAcquire();
		bool StopAcquire();
		bool IsReadyToAcquire();	
		void SendHeartbeat(int count);

		bool getDCResults(out DCMapVector dcmapVec);
	};

	interface HybridCameraManager
	{
		HybridProcessor* OpenCamera(CameraInfos cameraInfos);
		bool CloseCamera(CameraInfos cameraInfos);
		ProState GetHybridCameraState(CameraInfos cameraInfos);

		//writeFastq
		int generateFastQ(LaneParams laneInfo); 
		//check wether writefq finished and get fastq file path
		int checkFastqFinished(BatchId batch,out outFilesDict filesList); //batch.cycle=TotalCycle,retrun -1:not finished 1:SE 2:PE 3:SE+bar 4:PE+bar
        
	};

	//----add for V8 test

    };
	
	
	


	module PipelineMonitoring
    {
        sequence<byte> SinkRecord;

        interface SinkDestination
        {
            //["ami"] void publish(string sinkName, long timestamp, SinkRecord record); //ami implement in 3.6.2 Proxy.h
			void publish(string sinkName, long timestamp, SinkRecord record);
        };

        interface Aggregator
        {
            void subscribe(string topic, Object* client);
            void unsubscribe(Object* client);
        };
    };
	
	module ImageSave {
        const int ImageSaveSinkTypeId = 9;

        const short ImageSaveRecordVer = 1;

        struct ImageSaveRecord
        {
            short recordVersion;
            OSIIP::BatchId batch;
            int field;
            OSIIP::ImageDye dye;
			OSIIP::ImageChannel color;
            int fieldRow;
            int fieldColumn;
            OSIIP::ImageAttributes imageAttributes;
            OSIIP::ImageData imageData;
        };
    };
};