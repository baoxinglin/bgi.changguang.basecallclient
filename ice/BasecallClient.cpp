#include "BasecallClient.h"
#include <IceUtil\IceUtil.h>
#include <fstream>
#include <chrono>
#include "cppseh.hpp"

#pragma region BasecallCommand_impl

BasecallClient::BasecallStatus BasecallClient::GetBasecallStatus()
{
	if (!IsHostAlive())
	{
		return BasecallStatus::Offline;
	}

	ProState status = ice_GetHybridCameraState(_cameraInfos);

	switch (status)
	{
	case INACTIVE:
	case ACTIVE:
		return BasecallStatus::Ready;
	case ACQUIREWAIT:
	case ACQUIRING:
	case ACQUIRECOMPLETE:
	case PROCESSINGCOMPLETE:
		return BasecallStatus::Busy;
		break;
	default:
		return BasecallStatus::Disable;
	}
}

void BasecallClient::ResetBasecall()
{
	if (GetBasecallStatus() == BasecallStatus::Busy)
	{
		ice_StopAcquire();
		WaitingForReadyToAcquire(30000);
		ice_EndCycle();
	}
}

bool BasecallClient::OpenCamera()
{
	SetHostUnAlive();
	return CheckHostAlive();
}

bool BasecallClient::CloseCamera()
{
	CheckBasecallOnline();
	return _remoteCameraManagerPrx->CloseCamera(_cameraInfos);
}

void BasecallClient::CheckBasecallOnline()
{
	if (!CheckHostAlive())
	{
		throw std::exception("Basecall is disconnected.");
	}
}

int BasecallClient::ice_generateFQ(LaneParamsPtr laneInfo)
{
	CheckBasecallOnline();
	return _remoteCameraManagerPrx->generateFastQ(laneInfo);
}

int BasecallClient::ice_checkFQFinished(const BatchId& batch, outFilesDict& filesList)
{
	CheckBasecallOnline(); 
	return _remoteCameraManagerPrx->checkFastqFinished(batch, filesList);
}

ProState BasecallClient::ice_GetHybridCameraState(CameraInfos cameraInfos)
{
	CheckBasecallOnline(); 
	return _remoteCameraManagerPrx->GetHybridCameraState(cameraInfos);
}

bool BasecallClient::ice_SetCameraConfig(const CameraSettings& cameraSettings)
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->SetCameraConfig(cameraSettings);
}

bool BasecallClient::ice_CalibrateDarkFrame(const std::string & slide, Ice::Int cycleNum, Ice::Int picNum, bool saveToDisk)
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->CalibrateDarkFrame(slide, cycleNum, picNum, saveToDisk);
}

bool BasecallClient::ice_UseDarkFrameCalibration(bool use)
{
	CheckBasecallOnline();
	return _remoteProcessorPrx->UseDarkFrameCalibration(use);
}

bool BasecallClient::ice_StartCycle(CycleInfoPtr cycleInfo)
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->StartCycle(cycleInfo);
}

bool BasecallClient::ice_EndCycle()
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->EndCycle();
}

bool BasecallClient::ice_IsReadyToAcquire()
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->IsReadyToAcquire();
}

bool BasecallClient::ice_StartAcquire(FovList fovList, IMGProcessType IMGProType, bool secScan)
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->StartAcquire(fovList, IMGProType, secScan);
}

bool BasecallClient::ice_StopAcquire()
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->StopAcquire();
}

bool BasecallClient::ice_CompleteAcquire()
{
	CheckBasecallOnline(); 
	return _remoteProcessorPrx->CompleteAcquire();
}

void BasecallClient::ice_GetAndSaveDCResults(std::string genDCPath)
{
	CheckBasecallOnline();

	DCMapVector dcmapVec;
	bool ret = _remoteProcessorPrx->getDCResults(dcmapVec);
	EnableStopReconn();
	while (!ret)
	{
		if (CheckStopReconn())
		{
			break;
		}

		std::this_thread::sleep_for(std::chrono::seconds(1));
		ret = _remoteProcessorPrx->getDCResults(dcmapVec);
	}

	if (ret)
	{
		std::vector<std::string> keylist = { "u0","v0","fx","fy","k1","k2","k3","p1","p2","ifuse","avgDev" };
		std::ofstream fs(genDCPath, std::fstream::out);
		for (auto it_dc = dcmapVec.cbegin(); it_dc != dcmapVec.cend(); ++it_dc)
		{
			fs << "[Distortion]\n";
			for (std::vector<std::string>::const_iterator it = keylist.cbegin(); it != keylist.cend(); ++it)
			{
				if (*it == "avgDev")
				{
					fs << ";";
				}
				std::map<std::string, std::string>::const_iterator itm = it_dc->find(*it);
				if (itm != it_dc->cend())
				{
					fs << *it << "=" << itm->second << "\n";
				}
			}
		}
		CPP_LOG_INFO("GetandSaveDCResults success");
	}
	else
	{
		CPP_LOG_INFO("GetandSaveDCResults fail");
	}
}

#pragma endregion

BasecallClient::BasecallClient()
{

}

BasecallClient::~BasecallClient()
{

}

void BasecallClient::StartCycle(CycleInfoPtr cycInfo)
{
	_cycleInfo = cycInfo;
	_bCycleEnd = false;
	_ScanError.clear();
	_ScanInfo = ScanInfo();
	_thread = new std::thread([this](BasecallClient* client) {
		CPP_LOG_INFO("Start cycle");
		client->ScanFlow();
		_bCycleEnd = true;
		CPP_LOG_INFO("Cycle complete");
	}, this);
}

void BasecallClient::CheckStopCycle()
{
	if (_bStopCycle)
	{
		throw std::exception("Stop cycle from sbc");
	}
}

void BasecallClient::AbortCycle()
{
	try
	{
		CPP_LOG_ERROR("Abort cycle");
		ice_StopAcquire();
		WaitingForReadyToAcquire();
		ice_EndCycle();
	}
	catch (const std::exception& ex)
	{
		CPP_LOG_ERROR((std::string("Abort cycle fail : ") + ex.what()).c_str());
	}
	catch (...)
	{
		CPP_LOG_ERROR("Abort cycle fail : Unknown Exception");
	}
}

bool BasecallClient::WaitingForCycleEnd(unsigned int timeout_ms)
{
	int spendtime_ms = 0;

	while (true)
	{
		if (_bCycleEnd)
		{
			return true;
		}

		if (spendtime_ms >= timeout_ms)
		{
			return false;
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			spendtime_ms += 100;
		}
	}

	return false;
}

void BasecallClient::WaitingForReadyToAcquire(unsigned int timeout_ms)
{
	CPP_LOG_INFO("----Waiting for ready to acquire");

	int spendtime_ms = 0;

	while (true)
	{
		if (ice_IsReadyToAcquire())
		{
			CPP_LOG_INFO("----IsReadyToAcquire, spendtime = %d", spendtime_ms);
			return;
		}

		if (spendtime_ms >= timeout_ms)
		{
			CPP_LOG_ERROR("WaitingForReadyToAcquire timeout, spendtime = %d", spendtime_ms);
			throw std::exception("Waiting for ready to acquire fail");
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			spendtime_ms += 100;
			if (spendtime_ms % 500 == 0)
			{
				CPP_LOG_DEBUG("----Wait time = %d", spendtime_ms);
			}
		}

		CheckStopCycle();
	}
}

void BasecallClient::WaitingForScanDone(unsigned int timeout_ms)
{
	CPP_LOG_INFO("----Waiting for scan done");

	bool bScanComplete = true; //临时设置为true，等平台实现回调接口后改为false
	int spendtime_ms = 0;
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		if (!bScanComplete)
		{
			if (IsScanComplete())
			{
				CPP_LOG_INFO("----Scan complete, spendtime = %d", spendtime_ms);
				bScanComplete = true;
			}
		}
		else
		{
			if (IsDownloadComplete())
			{
				CPP_LOG_INFO("----Download complete, spendtime = %d", spendtime_ms);
				return;
			}
		}

		if (spendtime_ms >= timeout_ms)
		{
			CPP_LOG_ERROR("WaitingForScanDone timeout, spendtime = %d", spendtime_ms);
			throw std::exception("Waiting for scan done fail");
		}

		spendtime_ms += 10;
		if (spendtime_ms % 1000 == 0)
		{
			CPP_LOG_INFO("----WaitingForScanDone, spendtime = %d", spendtime_ms);
		}

		CheckStopCycle();
	}
}

void BasecallClient::WriteFq(LaneParamsPtr laneInfo, BatchId bid, outFilesDict files)
{
	CPP_LOG_INFO("generateFQ...");

	ice_generateFQ(laneInfo);
	ice_checkFQFinished(bid, files);

	if (files.size() > 0)
	{
		CPP_LOG_INFO("output fq.gz number: %d, file size() = %d", files.size(), files.begin()->second.size());
	}
	else
	{
		CPP_LOG_INFO("no files return");
	}
}

void BasecallClient::CreateLocalAdpter()
{
	_ident.name = IceUtil::generateUUID();
	_ident.category = "";
	ProcessorCallbackPtr localObj(new ProcessorCallbackI(this));
	_adapter = Communicator()->createObjectAdapter("");
	_adapter->add(localObj, _ident);
	_adapter->activate();
}

void BasecallClient::CreateRemoteProxy()
{
	CPP_LOG_INFO("Connnet to server...");

	//连接到服务器获取远程对象
	auto props = Communicator()->getProperties();
	Ice::ObjectPrx base = Communicator()->stringToProxy(props->getProperty("Identity") + std::string(":") + props->getProperty("pipeline.clientEndpoint"));
	Ice::ObjectPrx baseCB = Communicator()->stringToProxy(props->getProperty("IdentityCB") + std::string(":") + props->getProperty("pipeline.clientEndpointCallback"));

	ProcessorCallbackPrx remoteCallBackPrx = ProcessorCallbackPrx::checkedCast(baseCB);
	_remoteCameraManagerPrx = HybridCameraManagerPrx::checkedCast(base);

	if (!_remoteCameraManagerPrx || !remoteCallBackPrx)
	{
		_remoteCameraManagerPrx = nullptr;
		remoteCallBackPrx = nullptr;
		throw std::exception("Remote Object Not Exist");
	}

	//增加ICE连接对象的回调接口
	remoteCallBackPrx->ice_getConnection()->setCallback(Ice::ConnectionCallbackPtr(new ClientConnCallBackProc(this)));
	remoteCallBackPrx->ice_getConnection()->setACM(0, Ice::CloseOnIdleForceful, Ice::ACMHeartbeat::HeartbeatOff);

	//获得连接对象并将适配器传递给连接对象
	remoteCallBackPrx->ice_getConnection()->setAdapter(_adapter);

	//传递本地服务对象给服务器用于回调
	remoteCallBackPrx->AddClient(_ident);

	CPP_LOG_INFO("Connnet success");

	//打开相机
	static bool IsFirstConnect = true;
	if (IsFirstConnect)
	{
		IsFirstConnect = false;
		//_remoteCameraManagerPrx->CloseCamera(_cameraInfos);
	}
	_remoteProcessorPrx = _remoteCameraManagerPrx->OpenCamera(_cameraInfos);
	if (!_remoteProcessorPrx)
	{
		throw std::exception("OpenCameras Failed");
	}

	CPP_LOG_INFO("OpenCameras Success");
}

void BasecallClient::SetScanComplete(int nScanIndex)
{
	if (nScanIndex == _nScanIndex)
	{
		_bScanComplete = true;
	}
}

void BasecallClient::SetDownloadComplete(DownLoadResult result)
{
	//处理返回结果
	std::string strResult;
	if (result & DownLoadResult::None)				strResult += "None|";
	if (result & DownLoadResult::CameraOneLostOne)	strResult += "CameraOneLostOne|";
	if (result & DownLoadResult::CameraOneLostTwo)	strResult += "CameraOneLostTwo|";
	if (result & DownLoadResult::CameraTwoLostOne)	strResult += "CameraTwoLostOne|";
	if (result & DownLoadResult::CameraTwoLostTwo)	strResult += "CameraTwoLostTwo|";
	if (strResult.size() > 0)
	{
		CPP_LOG_ERROR("Error:SetFovLost->%s", strResult.c_str());
	}
	++_nDownloadComplete;
}
