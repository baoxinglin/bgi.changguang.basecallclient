#pragma once

#include <memory>
#include "ImageInterface.h"
#include "ProcessorCallbackImp.h"
#include "IceClient.h"

using namespace CGI::OSIIP;

class BasecallClient : public IceClient
{
public:
	BasecallClient();
	virtual ~BasecallClient();

	void StartCycle(CycleInfoPtr cycInfo);
	void StopCycle(){ _bStopCycle = true; }
	bool WaitingForCycleEnd(unsigned int timeout_ms = 1200000);
	void WriteFq(LaneParamsPtr laneInfo, BatchId bid, outFilesDict files);

	inline bool IsCycleEnd() { return _bCycleEnd; }
	inline void SetUseDarkframe(bool use) { _bUseDarkframe = use; }
	inline CameraInfos GetCameraInfos() { return _cameraInfos; }
	inline CameraSettings GetCameraSettings() { return _cameraSettings; }
	inline void SetCameraInfos(CameraInfos info) { _cameraInfos = info; }
	inline void SetCameraSettings(CameraSettings info) { _cameraSettings = info; }

	#pragma region BasecallManage

public:
	enum BasecallStatus
	{
		Offline		= 0,
		Ready		= 1,
		Busy		= 2,
		Disable		= 3,
	};
	BasecallStatus GetBasecallStatus();
	void ResetBasecall();
	bool OpenCamera();
	bool CloseCamera();

	#pragma endregion

	#pragma region BasecallCommand

protected:
	void CheckBasecallOnline();

	//HybridCameraManager
	int ice_generateFQ(LaneParamsPtr laneInfo);
	int ice_checkFQFinished(const BatchId& batch, outFilesDict& filesList);
	ProState ice_GetHybridCameraState(CameraInfos cameraInfos);

	//HybridProcessor
	bool ice_SetCameraConfig(const CameraSettings& cameraSettings);
	bool ice_CalibrateDarkFrame(const std::string& slide, Ice::Int cycleNum, Ice::Int picNum, bool saveToDisk);
	bool ice_UseDarkFrameCalibration(bool use);
	bool ice_StartCycle(CycleInfoPtr cycleInfo);
	bool ice_EndCycle();
	bool ice_IsReadyToAcquire();
	bool ice_StartAcquire(FovList fovList, IMGProcessType IMGProType, bool secScan);
	bool ice_StopAcquire();
	bool ice_CompleteAcquire();

	//TestDC
	void ice_GetAndSaveDCResults(std::string genDCPath);

	#pragma endregion

	#pragma region ScanControl

public:
	enum ScanStatus
	{
		Invalid = 0xff,
		Prepare = 1,
		Scan = 2,
		Process = 3,
		Complete = 0,
		Fail = -1
	};
	struct ScanInfo
	{
		ScanStatus status;
		int row;
		int col;
		ScanInfo()
		{
			status = ScanStatus::Invalid;
			row = 0;
			col = 0;
		}
		ScanInfo(ScanStatus sta, int r = 0, int c = 0)
		{
			status = sta;
			row = r;
			col = c;
		}
	};
	ScanInfo GetScanInfo() { return _ScanInfo; }
	std::string GetScanError() { return _ScanError; }

	void SetScanComplete(int nScanIndex);
	void SetDownloadComplete(DownLoadResult result);

protected:
	inline void ResetScanComplete(int nScanIndex) { _bScanComplete = false; _nScanIndex = nScanIndex; }
	inline void ResetDownloadComplete(int nMaxDownloadCount) { _nDownloadComplete = 0; _nMaxDownloadCount = nMaxDownloadCount; }
	inline bool IsScanComplete() { return _bScanComplete; }
	inline bool IsDownloadComplete() { return (_nDownloadComplete < _nMaxDownloadCount) ? false : true; }
	inline int  GetDownloadComplete() { return _nDownloadComplete; }
	
	void WaitingForReadyToAcquire(unsigned int timeout_ms = 30000);
	void WaitingForScanDone(unsigned int timeout_ms = 30000);

	void CheckStopCycle();
	void AbortCycle();

	#pragma endregion

protected:
	virtual void CreateLocalAdpter() override;
	virtual void CreateRemoteProxy() override;
	virtual void ScanFlow() = 0;

protected:
	CameraInfos _cameraInfos;
	CameraSettings _cameraSettings;
	CycleInfoPtr _cycleInfo = new CycleInfo();
	std::atomic<ScanInfo> _ScanInfo = ScanInfo();
	std::string _ScanError;
	bool _bUseDarkframe = false;

private:
	std::thread* _thread = nullptr;
	std::atomic_bool _bCycleEnd = false;
	std::atomic_bool _bStopCycle = false;
	std::atomic_bool _bScanComplete = false;
	std::atomic_uint _nScanIndex = 0;
	std::atomic_uint _nDownloadComplete = 0;
	std::atomic_uint _nMaxDownloadCount = 0;
	Ice::Identity _ident;
	Ice::ObjectAdapterPtr _adapter = nullptr;
	HybridCameraManagerPrx _remoteCameraManagerPrx = nullptr;
	HybridProcessorPrx _remoteProcessorPrx = nullptr;
};

typedef std::shared_ptr<BasecallClient> BasecallClientPtr;
