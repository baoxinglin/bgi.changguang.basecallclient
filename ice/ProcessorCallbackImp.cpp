#include "BasecallClient.h"
#include "ProcessorCallbackImp.h"

ProcessorCallbackI::ProcessorCallbackI(BasecallClient* client)
	: _client(client)
{
}

ProcessorCallbackI::~ProcessorCallbackI()
{
}

void ProcessorCallbackI::AddClient(const::Ice::Identity &identy, const::Ice::Current &cur)
{
}

void ProcessorCallbackI::DownLoadComplete(::CGI::OSIIP::DownLoadResult ret, const::Ice::Current &)
{
	_client->SetDownloadComplete(ret);
}

void ProcessorCallbackI::SendImage(const::CGI::OSIIP::ImageData &, const::CGI::OSIIP::ImageMetadata &, const::Ice::Current &)
{
}

void ProcessorCallbackI::SendCoarseThetaResult(const::CGI::OSIIP::FOVInformation &, const::CGI::OSIIP::RegResults &, const::Ice::Current &)
{
}

void ProcessorCallbackI::SendHeartbeat(::Ice::Int count, const::Ice::Current &)
{

}

void ProcessorCallbackI::SendImageMetrics(const ::CGI::OSIIP::FOVInformation&, const ::CGI::OSIIP::ImageMetricsResults&, const::Ice::Current &)
{

}

void ProcessorCallbackI::SendBubbleDetectResult(const CGI::OSIIP::fovListBubbleDetectResult &, const Ice::Current &)
{
}
