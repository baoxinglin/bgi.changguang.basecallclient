#pragma once

#include "ImageInterface.h"

class BasecallClient;

class ProcessorCallbackI : public CGI::OSIIP::ProcessorCallback
{
public:
	ProcessorCallbackI(BasecallClient* client);
	~ProcessorCallbackI();

	// Inherited via ProcessorCallback
	virtual void AddClient(const ::Ice::Identity &ident, const ::Ice::Current& cur = ::Ice::Current()) override;
	virtual void DownLoadComplete(::CGI::OSIIP::DownLoadResult ret, const::Ice::Current & cur = ::Ice::Current()) override;
	virtual void SendImage(const::CGI::OSIIP::ImageData &, const::CGI::OSIIP::ImageMetadata &, const::Ice::Current & cur = ::Ice::Current()) override;
	virtual void SendCoarseThetaResult(const::CGI::OSIIP::FOVInformation &, const::CGI::OSIIP::RegResults &, const::Ice::Current & cur = ::Ice::Current()) override;
	virtual void SendHeartbeat(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendImageMetrics(const ::CGI::OSIIP::FOVInformation&, const ::CGI::OSIIP::ImageMetricsResults&, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendBubbleDetectResult(const CGI::OSIIP::fovListBubbleDetectResult &, const Ice::Current & = Ice::Current()) override;

private:
	BasecallClient* _client = nullptr;
};

