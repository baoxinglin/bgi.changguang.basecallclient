#include "IceClient.h"
#include <IceUtil\IceUtil.h>
#include <fstream>
#include <chrono>

IceClient::IceClient()
	: _sem(1)
{
	CPP::CppLogger::Initialize(CPP::CppLogger::LOG_ALL_OUT, 31, "log");
	CPP::CppLogger::Instance()->EnableDetail(true);
}

IceClient::~IceClient()
{
	_bStopReconn = true;
	_bClientExit = true;
	_sem.Signal();

	if (_thread.joinable())
	{
		_thread.join();
	}

	if (_ic)
	{
		try
		{
			_ic->shutdown();
			_ic->waitForShutdown();
			_ic->destroy();
			CPP_LOG_INFO("IceClient Destory");
		}
		catch (const std::exception& e)
		{
			CPP_LOG_ERROR("%s", e.what());
		}
		catch (...)
		{
			CPP_LOG_ERROR("Unknown Exception");
		}
	}
}

bool IceClient::Initialize()
{
	try
	{
		//创建Ice环境
		_ic = Ice::initialize();

		//加载配置文件
		auto props = _ic->getProperties();
		props->load("settings.config");

		//创建本地回调接口对象
		CreateLocalAdpter();

		//创建连接监测线程
		auto MonitorThread = [this]()
		{
			while (true)
			{
				_sem.Wait(100);
				while (!_bClientExit && !_bHostAlive)
				{
					Connect();
				}
				if (_bClientExit)
				{
					return;
				}
			}
		};

		_thread = std::thread(MonitorThread);

		CPP_LOG_INFO("IceClient Create");

		return true;
	}
	catch (const std::exception& e)
	{
		CPP_LOG_ERROR("%s", e.what());
	}
	catch (...)
	{
		CPP_LOG_ERROR("Unknown Exception");
	}

	return false;
}

void IceClient::SetHostUnAlive()
{
	std::lock_guard<std::mutex> lock(_mtConn);
	_bHostAlive = false;
	_sem.Signal();
}

bool IceClient::Connect()
{
	std::lock_guard<std::mutex> lock(_mtConn);

	if (_bClientExit || _bStopReconn)
	{
		return false;
	}

	if (_bHostAlive)
	{
		return true;
	}

	try
	{
		CreateRemoteProxy();
		_bHostAlive = true;
		return true;
	}
	catch (const std::exception& e)
	{
		CPP_LOG_ERROR("%s", e.what());
	}
	catch (...)
	{
		CPP_LOG_ERROR("Unknown Exception");
	}

	return false;
}

bool IceClient::CheckHostAlive(int nMaxWaitTime_ms)
{
	EnableStopReconn();

	int timeout_ms = 0;
	while (!_bHostAlive)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		timeout_ms += 10;

		if (CheckStopReconn() || timeout_ms > nMaxWaitTime_ms)
		{
			break;
		}
	}

	return _bHostAlive;
}
