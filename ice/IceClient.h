#pragma once

#include <mutex>
#include <atomic>
#include <thread>
#include <functional>
#include <Ice/Ice.h>
#include <Ice\Connection.h>
#include "../CppLib/cpplogger.h"
#include "../CppLib/cppsemaphore.h"

class IceClient
{
protected:
	class ClientConnCallBackProc : public Ice::ConnectionCallback
	{
	public:
		ClientConnCallBackProc(IceClient* client) : _ptrClient(client)
		{

		}

		void heartbeat(const ::Ice::ConnectionPtr& conn)
		{
			CPP_LOG_INFO("BasecallClient Hearbeat, times = %d", _nHeartbeatCount++);
		}

		void closed(const ::Ice::ConnectionPtr& conn)
		{
			_ptrClient->SetHostUnAlive();
			CPP_LOG_INFO("IceClient Close");
		}

	private:
		IceClient* _ptrClient = nullptr;
		unsigned int _nHeartbeatCount = 0;
	};

public:
	bool Initialize();
	inline void StopReconn() { _bStopReconn = true; }
	inline bool IsHostAlive() { return _bHostAlive; }
	bool CheckHostAlive(int nMaxWaitTime_ms = 120000);

protected:
	IceClient();
	virtual ~IceClient();

	void SetHostUnAlive();
	bool Connect();

	inline Ice::CommunicatorPtr Communicator() { return _ic; }
	inline void EnableStopReconn() { _bStopReconn = false; }
	inline bool CheckStopReconn() { return _bStopReconn; }

	virtual void CreateLocalAdpter() = 0;
	virtual void CreateRemoteProxy() = 0;

private:
	std::atomic_bool _bClientExit = ATOMIC_VAR_INIT(false);
	std::atomic_bool _bStopReconn = ATOMIC_VAR_INIT(false);
	std::atomic_bool _bHostAlive = ATOMIC_VAR_INIT(false);
	Ice::CommunicatorPtr _ic;
	std::thread _thread;
	std::mutex _mtConn;
	CPP::Semaphore _sem;
};

typedef std::shared_ptr<IceClient> IceClientPtr;
