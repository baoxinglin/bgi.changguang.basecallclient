﻿#pragma once

/// 线程安全队列

#include <queue>
#include <mutex>

namespace CPP {

	template<class T>
	class CppQueue
	{
	public:
		typedef std::lock_guard<std::mutex> mtlocker;

		/// 构造函数
		CppQueue() {}

		/// 析构函数
		virtual ~CppQueue() {}

		/// 向队列尾部插入数据
		int Push(T value) {
			mtlocker lock(_mt);
			_queue.push(value);
			return static_cast<int>(_queue.size());
		}

		/// 删除队列首部数据
		int Pop() {
			mtlocker lock(_mt);
			_queue.pop();
			return static_cast<int>(_queue.size());
		}

		/// 读取队首数据
		T& Front() {
			mtlocker lock(_mt);
			return _queue.front();
		}

		/// 读取队尾数据
		T& Back() {
			mtlocker lock(_mt);
			return _queue.back();
		}

		/// 删除队首数据并返回其拷贝
		T TakeFront() {
			mtlocker lock(_mt);
			T it = _queue.front();
			_queue.pop();
			return it;
		}

		/// 尝试读取队首数据
		bool TryTakeFront(T& t) {
			mtlocker lock(_mt);
			if (_queue.size() > 0) {
				t = _queue.front();
				_queue.pop();
				return true;
			}
			return false;
		}

		/// 查询队列长度
		int Size() {
			mtlocker lock(_mt);
			return static_cast<int>(_queue.size());
		}

		/// 清除队列数据
		void Clear() {
			mtlocker lock(_mt);
			while (_queue.size() > 0) {
				_queue.pop();
			}
		}

		/// 判断队列是否为空
		bool IsEmpty() {
			mtlocker lock(_mt);
			return _queue.empty();
		}

		/// 访问内部队列，非线程安全，配合Lock()和Unlock()函数可以访问队列全部数据
		std::queue<T>& GetQueue() { return _queue; }
		void Unlock() { _mt.unlock(); }
		void Lock() { _mt.lock(); }

	private:
		std::queue<T> _queue;
		std::mutex _mt;
	};

}
