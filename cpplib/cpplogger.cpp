﻿#include "cpplogger.h"
#include "cppfilesystem.h"
#include "cppqueue.hpp"
#include <stdarg.h>
#include <fstream>
#include <time.h>
#include <mutex>
#include <map>

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable:4091)
#include <Windows.h>
#include <Dbghelp.h>
#include <atlbase.h>
#include <atlstr.h>
#pragma comment(lib,"Dbghelp.lib")
#pragma warning(pop)
#else
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <thread>
#endif

namespace CPP {

using namespace std;

class CppLoggerManager
{
public:
    CppLoggerManager() {
    }

    ~CppLoggerManager() {
        auto it = _map.begin();
        while (it != _map.end()) {
            delete it->second;
            ++it;
        }
    }

    static string DateTime(string& filename) {
        char name[16] = {0};
        char buf[256] = {0};
#ifdef _WIN32
        SYSTEMTIME tm;
        GetLocalTime(&tm);
        sprintf(name, "%d-%02d-%02d", tm.wYear, tm.wMonth, tm.wDay);
        sprintf(buf, "[%d-%02d-%02d %02d:%02d:%02d.%03d]", tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond, tm.wMilliseconds);
#else
        //need linux code
        time_t tt = time(NULL);
        struct tm* t= localtime(&tt);
        struct timeval tv;
        gettimeofday(&tv, NULL);
        sprintf(name, "%d-%02d-%02d", t->tm_year+1900, t->tm_mon+1, t->tm_mday);
        sprintf(buf, "%02d:%02d:%02d:%03d", t->tm_hour, t->tm_min, t->tm_sec, tv.tv_usec/1000);
#endif
        filename.append(name);
        return string(buf);
    }

    void MakeLogDir() {
        if (!CPP::CppFileSystem::Exist(_logDir)) {
            CPP::CppFileSystem::MakeDir(_logDir);
        }
    }

public:
    map<string, CppLogger*> _map;
    string _logDir = "log";
    int _logMode = 0;
    int _logFilter = 0;
	mutex _mt;
};

CppLoggerManager* g_logManager = nullptr;

class CppLoggerPrivate
{
public:
	CppLoggerPrivate(const string& szLogName) :_logName(szLogName) {
		_map[CppLogger::LV_INFO] = "INFO";
		_map[CppLogger::LV_WARNING] = "WARN";
		_map[CppLogger::LV_ERROR] = "ERROR";
		_map[CppLogger::LV_FATAL] = "FATAL";
		_map[CppLogger::LV_DEBUG] = "DEBUG";
		_logMode = g_logManager->_logMode;
		_logFilter = g_logManager->_logFilter;
		_logDir = g_logManager->_logDir;

		string strLogData;
		CppLoggerManager::DateTime(strLogData);
		char szFileName[256] = { 0 };
		sprintf(szFileName, "%s//%s%s.log", _logDir.c_str(), (_logName.empty() ? "" : (_logName + "_").c_str()), strLogData.c_str());
		_fileName = szFileName;

		_thread = new thread([](CppLoggerPrivate* loggerPrivate) { loggerPrivate->LogThreadProc(); }, this);
    }

	~CppLoggerPrivate() {
		if (_thread != nullptr) {
			_bExit = true;
			_thread->join();
			delete _thread;
		}
	}

	void AddLog(const string& strText) {
		_queue.Push(strText);
	}

	void LogThreadProc() {
		_out.open(_fileName, ios::app | ios::out);
		if (!_out.is_open()) {
			cerr << "Open log file fail, logName=" << _logName << ", filePath=" << _fileName << endl;
			return;
		}

		bool isAddLog = false;
		string strText;
		while (!_bExit) {
			if (_queue.TryTakeFront(strText)) {
				_out.write(strText.c_str(), strText.size());
				isAddLog = true;
			}
			else {
				if (isAddLog) {
					isAddLog = false;
					_out.flush();
				}
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
		}

		while (_queue.TryTakeFront(strText)) {
			_out.write(strText.c_str(), strText.size());
		}

		_out.flush();
		_out.close();
	}

public:
	string _map[CppLogger::LV_MAX];
    string _logDir;
    string _logName;
    string _fileName;
	fstream _out;
    mutex _mt;
    int _logMode = 0;
    int _logFilter = 0;
	bool _bExit = false;
	bool _logDetail = true;
	CppQueue<string> _queue;
	std::thread* _thread = nullptr;
};

CppLogger::CppLogger() : _p(new CppLoggerPrivate("default"))
{
}

CppLogger::CppLogger(const string &name) : _p(new CppLoggerPrivate(name))
{
}

CppLogger::~CppLogger()
{
	delete _p;
}

void CppLogger::Initialize(int logMod, int logFilter, const string &logDir)
{
    if (nullptr == g_logManager) {
        g_logManager = new CppLoggerManager;
        g_logManager->_logDir = logDir;
        g_logManager->_logMode = logMod & 0x03;
        g_logManager->_logFilter = logFilter & 0x1f;
		g_logManager->MakeLogDir();
    }
}

void CppLogger::Uninitialize()
{
    if (g_logManager != nullptr) {
        delete g_logManager;
        g_logManager = nullptr;
    }
}

CppLogger *CppLogger::Instance(const string &name)
{
    if (g_logManager != nullptr) {
		std::lock_guard<std::mutex> lock(g_logManager->_mt);
        string find_name = name.empty() ? "default" : name;
        auto it = g_logManager->_map.find(find_name);
        if (it != g_logManager->_map.end()) {
            return it->second;
        }
        else {
            CppLogger* newlogger = new CppLogger(name);
            g_logManager->_map[find_name] = newlogger;
            return newlogger;
        }
    }
    return nullptr;
}

string CppLogger::GetFileName()
{
    return _p->_fileName;
}

void CppLogger::EnableDetail(bool bOutputLogDetail)
{
	_p->_logDetail = bOutputLogDetail;
}

void CppLogger::WriteLog(LOG_LEVEL level, int lineno, const char *filepath, const char *format, ...)
{
    if (level & _p->_logFilter) {
        //不定参数格式化
        va_list argptr;
        va_start(argptr, format);
        char msgbuff[3800] = {0};
        vsprintf(msgbuff, format, argptr);
        va_end(argptr);

        //获取当前日期
        string date;
        string time = CppLoggerManager::DateTime(date);

        //格式化输出
        char outbuff[4096] = {0};
		const char* szFmt = _p->_logDetail ? "%s %s	>> %s #%d\n" : "%s %s\n";
		int size = sprintf(outbuff, szFmt, time.c_str(), msgbuff, filepath, lineno);

        //输出到标准控制台
        if (_p->_logMode&LOG_STD_OUT) {
#ifdef _Win32_OutputDebugString
            OutputDebugStringA(CA2A(outbuff));
#else
            std::lock_guard<std::mutex> lock(g_logManager->_mt);
            if (LV_ERROR==level || LV_FATAL==level) {
                cerr << outbuff;
            }
            else {
                cout << outbuff << flush;
            }
#endif
        }

        //输出到日志文件
		if (_p->_logMode & LOG_FILE_OUT) {
			_p->AddLog(outbuff);
		}
    }
}

}
