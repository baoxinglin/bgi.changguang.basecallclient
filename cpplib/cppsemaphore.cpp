﻿#include "cppsemaphore.h"

typedef std::unique_lock<std::mutex> semlock;

namespace CPP {

class SemaphorePrivate
{
public:
	SemaphorePrivate(unsigned int sig_initial = 0) : _sigcount(sig_initial) { }
	int _sigcount;
	std::mutex _mt;
	std::condition_variable _condi;
};

Semaphore::Semaphore(unsigned int count)
	: _p(new SemaphorePrivate(count))
{
}

Semaphore::~Semaphore()
{
    delete _p;
}

void Semaphore::Clear()
{
    semlock lock(_p->_mt);
    _p->_sigcount = 0;
}

void Semaphore::Signal(unsigned int sigs)
{
    if (sigs > 1) {
        semlock lock(_p->_mt);
        _p->_sigcount += sigs;
        _p->_condi.notify_all();
    }
    else if (sigs > 0) {
        semlock lock(_p->_mt);
        ++_p->_sigcount;
        _p->_condi.notify_one();
    }
    else {
    }
}

bool Semaphore::Wait(int timeout)
{
	semlock lock(_p->_mt);

	if (timeout < 0) {
		while (0 == _p->_sigcount) {
			_p->_condi.wait(lock);
		}
		--_p->_sigcount;
		return true;
	}
	else if (0 == timeout) {
		if (_p->_sigcount > 0) {
			--_p->_sigcount;
			return true;
		}
		return false;
	}
	else
	{
		if (_p->_sigcount > 0) {
			--_p->_sigcount;
			return true;
		}
		else {
			if (_p->_condi.wait_for(lock, std::chrono::milliseconds(timeout)) != std::cv_status::timeout) {
				if (_p->_sigcount > 0) {
					--_p->_sigcount;
					return true;
				}
			}
		}
	}

	return false;
}

bool Semaphore::TryWait()
{
	return Wait();
}

}
