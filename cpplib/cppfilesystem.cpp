﻿#include "cppfilesystem.h"

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable:4091)
#include <Windows.h>
#include <Dbghelp.h>
#include <atlbase.h>
#include <atlstr.h>
#include <direct.h>
#include <io.h>
#pragma comment(lib,"Dbghelp.lib")
#pragma warning(pop)
#else
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#endif

namespace CPP {

	using namespace std;

	CppFileSystem::CppFileSystem()
	{

	}

	CppFileSystem::~CppFileSystem()
	{

	}

	bool CppFileSystem::Exist(const std::string &fileName)
	{
		return (access(fileName.c_str(), 0) != 0) ? false : true;
	}

	bool CppFileSystem::MakeDir(const string &dirPath)
	{
#ifdef _WIN32
		//return (mkdir(dirPath.c_str())!=0) ? false : true;
		//create full path dir
		string dir = dirPath + "\\";
		MakeSureDirectoryPathExists(dir.c_str());
#else
		//return (mkdir(dirPath.c_str(), 0777)!=0) ? false : true;
		//create full path dir
		string cmd = "mkdir -p " + dirPath;
		system(cmd.c_str());
#endif
		return Exist(dirPath);
	}

	bool CppFileSystem::RemoveDir(const string &folderPath)
	{
#ifdef _WIN32
		try {
			_finddata_t FileInfo;
			std::string findPath = folderPath + "\\*.*";
			auto handle = _findfirst(findPath.c_str(), &FileInfo);
			if (handle != 0) {
				do {
					if (FileInfo.attrib & _A_SUBDIR) {
						if ((strcmp(FileInfo.name, ".") != 0) && (strcmp(FileInfo.name, "..") != 0)) {
							string newFolderPath = folderPath + "\\" + FileInfo.name;
							RemoveDir(newFolderPath);
						}
					}
					else {
						string fileName = folderPath + "\\" + FileInfo.name;
						remove(fileName.c_str());
					}
				} while (_findnext(handle, &FileInfo) == 0);
				_findclose(handle);
				rmdir(folderPath.c_str());
				return true;
			}
		}
		catch (const std::exception& e) {
			std::cerr << "Exception : " << e.what() << std::endl;
		}
		catch (...) {
			std::cerr << "Exception : unknown error" << std::endl;
		}
#else
		try {
			DIR *dp;
			struct dirent *entry;
			struct stat statbuf;
			if ((dp = opendir(folderPath.c_str())) == NULL) {
				std::cerr << "cannot open directory : " << folderPath << std::endl;
				return false;
			}
			chdir(folderPath.c_str());
			while ((entry = readdir(dp)) != NULL) {
				lstat(entry->d_name, &statbuf);
				if (S_ISDIR(statbuf.st_mode)) {
					if (strcmp(".", entry->d_name) != 0 && strcmp("..", entry->d_name) != 0) {
						RemoveDir(entry->d_name);
					}
				}
				else {
					remove(entry->d_name);
				}
			}
			chdir("..");
			closedir(dp);
			rmdir(folderPath.c_str());
			return true;
		}
		catch (const std::exception& e) {
			std::cerr << "Exception : " << e.what() << std::endl;
		}
		catch (...) {
			std::cerr << "Exception : unknown error" << std::endl;
		}
#endif
		return false;
	}

	bool CppFileSystem::RemoveFile(const string &fileName)
	{
		return (remove(fileName.c_str()) != 0) ? false : true;
	}

	bool CppFileSystem::RenameFile(const string &oldFileName, const string &newFileName)
	{
		return (rename(oldFileName.c_str(), newFileName.c_str()) != 0) ? false : true;
	}

	string CppFileSystem::GetWorkDir()
	{
		char buffer[512] = { 0 };
		return getcwd(buffer, 512);
	}

	bool CppFileSystem::FindFiles(FilesList& filesList, const std::string& folderPath, CompareFunc compare, bool bFullPath, int depth)
	{
#ifdef WIN32
		try {
			_finddata_t FileInfo;
			std::string findPath = folderPath + "\\*.*";
			auto handle = _findfirst(findPath.c_str(), &FileInfo);
			if (handle != -1) {
				do {
					if (FileInfo.attrib & _A_SUBDIR) {
						if ((strcmp(FileInfo.name, ".") != 0) && (strcmp(FileInfo.name, "..") != 0)) {
							string newFolderPath = folderPath + "\\" + FileInfo.name;
							FindFiles(filesList, newFolderPath, compare, bFullPath, depth);
						}
					}
					else {
						string fileName = bFullPath ? (folderPath + "\\" + FileInfo.name) : FileInfo.name;
						if (compare != nullptr) {
							if (compare(std::string(FileInfo.name))) {
								filesList.push_back(fileName);
							}
						}
						else {
							filesList.push_back(fileName);
						}
					}
				} while (_findnext(handle, &FileInfo) == 0);
				_findclose(handle);
				return true;
			}
		}
		catch (const std::exception& e) {
			std::cerr << "Exception : " << e.what() << std::endl;
		}
		catch (...) {
			std::cerr << "Exception : unknown error" << std::endl;
		}
#else
		try {
			DIR *dp;
			struct dirent *entry;
			struct stat statbuf;
			if ((dp = opendir(folderPath.c_str())) == NULL) {
				std::cerr << "cannot open directory : " << folderPath << std::endl;
				return false;
			}
			std::string strWorkDir = CPP::CppFileSystem::GetWorkDir();
			chdir(folderPath.c_str());
			while ((entry = readdir(dp)) != NULL) {
				lstat(entry->d_name, &statbuf);
				if (S_ISDIR(statbuf.st_mode)) {
					if (strcmp(".", entry->d_name) != 0 && strcmp("..", entry->d_name) != 0) {
						string newFolderPath = folderPath + "/" + entry->d_name;
						FindFiles(filesList, newFolderPath, compare, bFullPath, depth + 4);
					}
				}
				else {
					string fileName = bFullPath ? (folderPath + "/" + entry->d_name) : entry->d_name;
					if (compare != nullptr) {
						if (compare(std::string(entry->d_name))) {
							filesList.push_back(fileName);
						}
					}
					else {
						filesList.push_back(fileName);
					}
				}
			}
			closedir(dp);
			chdir(strWorkDir.c_str());
			return true;
		}
		catch (const std::exception& e) {
			std::cerr << "Exception : " << e.what() << std::endl;
		}
		catch (...) {
			std::cerr << "Exception : unknown error" << std::endl;
		}
#endif
		return false;
	}

}
