#pragma once

#include <iostream>
#include <Windows.h>

//设置结构化异常处理函数
#define SET_SEH_EXCEPTION_HANDLE _set_se_translator(SEH_Exception::SE_Handle)

//C++结构化异常处理类
class SEH_Exception : public std::exception
{
public:
	SEH_Exception(const char* message) : std::exception(message) { }
	static void SE_Handle(unsigned int u, EXCEPTION_POINTERS* pExp)
	{
		char message[4096] = { 0 };
		TCHAR wch_message[4096] = { 0 };
		wsprintf(wch_message, L"Struct Exception:\nCode=%#x\n%Address=%#I64x\n",
			(unsigned long long)pExp->ExceptionRecord->ExceptionCode,
			(unsigned long long)pExp->ExceptionRecord->ExceptionAddress);
		throw SEH_Exception(WString2String(wch_message).c_str());
	}
private:
	static std::string WString2String(const std::wstring& ws)
	{
		std::string strLocale = setlocale(LC_ALL, "");
		const wchar_t* wchSrc = ws.c_str();
		size_t nDestSize = wcstombs(NULL, wchSrc, 0) + 1;
		char *chDest = new char[nDestSize];
		memset(chDest, 0, nDestSize);
		wcstombs(chDest, wchSrc, nDestSize);
		std::string strResult = chDest;
		delete[]chDest;
		setlocale(LC_ALL, strLocale.c_str());
		return strResult;
	}
};
