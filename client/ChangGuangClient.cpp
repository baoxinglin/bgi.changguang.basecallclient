#include "ChangGuangClient.h"
#include "cpplogger.h"
#include "cppseh.hpp"

ChangGuangClient* _CGClient = nullptr;
void ScanCompleteCallBack(int index)
{
	_CGClient->SetScanComplete(index);
}

ChangGuangClient::ChangGuangClient(CgStageInterfacePtr stage)
	: _stage(stage)
{
	SET_SEH_EXCEPTION_HANDLE;

	_CGClient = this;
	stage->SetScanDoneCallBack(ScanCompleteCallBack);

	const CameraChannels channels[] = { CameraChannels::ChannelA, CameraChannels::ChannelT,CameraChannels::ChannelG,CameraChannels::ChannelC };

	for (auto i = 0; i < 4; i++)
	{
		// Camera infos set
		CameraInfo camInfo = { 0 };
		camInfo.cameraIndex = i;
		camInfo.cameraType = ChangGuangCamera;
		_cameraInfos.push_back(camInfo);

		// Camera settings set
		CameraSetting camSet = { 0 };
		camSet.bitsPerPixel = 16;
		camSet.cameraInfo = camInfo;
		camSet.exposureNum = 1;
		camSet.exposureTime = 15;
		camSet.xResolution = 4896;
		camSet.yResolution = 4896;
		camSet.cameraMode = CameraMode::HardwareTriggerCamera;
		camSet.cameraChannelInfo = channels[i];
		_cameraSettings.push_back(camSet);
	}
}

ChangGuangClient::~ChangGuangClient()
{
}

#pragma region SaveToBasecall

void ChangGuangClient::SetSaveImgCol(unsigned int col, bool newVal)
{
	if (col < MAX_SAVE_IMG_CNT)
	{
		_SaveImgCols[col] = newVal;
	}
	else
	{
		throw std::exception("SetSaveImgCol out of range");
	}
}

void ChangGuangClient::SetSaveImgRow(unsigned int row, bool newVal)
{
	if (row < MAX_SAVE_IMG_CNT)
	{
		_SaveImgRows[row] = newVal;
	}
	else
	{
		throw std::exception("SetSaveImgRow out of range");
	}
}

bool ChangGuangClient::GetSaveImgCol(unsigned int col)
{
	if (col < MAX_SAVE_IMG_CNT)
	{
		return _SaveImgCols[col];
	}
	else
	{
		throw std::exception("GetSaveImgCol out of range");
	}
}

bool ChangGuangClient::GetSaveImgRow(unsigned int row)
{
	if (row < MAX_SAVE_IMG_CNT)
	{
		return _SaveImgRows[row];
	}
	else
	{
		throw std::exception("GetSaveImgRow out of range");
	}
}

#pragma endregion

void ChangGuangClient::ScanFlow()
{
	int iCurRow = 0;
	try
	{
		//进入扫描准备状态
		_ScanInfo = ScanInfo(ScanStatus::Prepare);

		//设置相机参数
		CPP_LOG_INFO("Set camera config");
		if (!ice_SetCameraConfig(_cameraSettings))
		{
			throw std::exception("Set camera config fail");
		}

		CheckStopCycle();

		//背景拍照模式
		if (_bUseDarkframe)
		{
			//计算DarkFrame背景并开启减背景
			CPP_LOG_INFO("Calibrate darkframe");
			if (!ice_CalibrateDarkFrame(_cycleInfo->slideId, _cycleInfo->cycle, 10, false))
			{
				throw std::exception("Calibrate darkframe fail");
			}

			//启用DarkFrame模式
			CPP_LOG_INFO("Set use darkframe");
			if (!ice_UseDarkFrameCalibration(true))
			{
				throw std::exception("Set use darkframe fail");
			}
		}

		CheckStopCycle();

		//开始Cycle循环
		CPP_LOG_INFO("Start cycle");
		if (!ice_StartCycle(_cycleInfo))
		{
			throw std::exception("Start cycle fail");
		}

		//扫描整个Cycle
		const int nRows = _cycleInfo->rowNums;
		const int nCols = _cycleInfo->colNums;
		for (iCurRow = 0; iCurRow < nRows; iCurRow++)
		{
			FovList fovList;
			bool bSaveImgRow = GetSaveImgRow(iCurRow);
			for (auto col = 0; col < nCols; col++)
			{
				FOVInformation fovInfo;
				fovInfo.col = (iCurRow % 2 == 0) ? col + 1 : nCols - col;
				fovInfo.row = iCurRow + 1;
				fovInfo.fovTypeId = 0;
				fovInfo.fovQuadrantId = 0;
				fovInfo.imagesPerFOV = 4;

				ImageInformation imgInfo;
				imgInfo.imgType = (ImageTransferType)((bSaveImgRow & GetSaveImgCol(fovInfo.col - 1)) ? ImageTransferType::BaseCall | ImageTransferType::SaveToDisk : ImageTransferType::BaseCall);
				//imgInfo.imgType = (ImageTransferType)((bSaveImgCol & GetSaveImgRow(row)) ? ImageTransferType::BaseCall | ImageTransferType::SaveToDisk : ImageTransferType::BaseCall);
				//imgInfo.imgType = ImageTransferType::Discard;
				imgInfo.isReceived = false;
				imgInfo.index = 0;
				imgInfo.cameraIndex = 0;
				imgInfo.imgColor = ImageChannel::Channel0; // A
				fovInfo.images.push_back(imgInfo);
				imgInfo.cameraIndex = 1;
				imgInfo.imgColor = ImageChannel::Channel3; // T
				fovInfo.images.push_back(imgInfo);
				imgInfo.cameraIndex = 2;
				imgInfo.imgColor = ImageChannel::Channel2; // G
				fovInfo.images.push_back(imgInfo);
				imgInfo.cameraIndex = 3;
				imgInfo.imgColor = ImageChannel::Channel1; // C
				fovInfo.images.push_back(imgInfo);
				fovList.push_back(fovInfo);
			}

			CheckStopCycle();

			//重置Scan和Download状态
			ResetScanComplete(iCurRow);
			ResetDownloadComplete(nRows);

			//进入扫描执行状态
			_ScanInfo = ScanInfo(ScanStatus::Scan, iCurRow + 1);

			//等待Basecall准备就绪
			WaitingForReadyToAcquire(60000);

			//开始扫描当前行
			CPP_LOG_INFO("----Start acquire scan row [ %d ]", iCurRow + 1);
			if (!ice_StartAcquire(fovList, IMGProcessType::BASECALLGROUPIMG, false))
			{
				throw std::exception("Start acquire fail");
			}

			//触发平台拍照
			_stage->Start(1, 1, nRows);

			//等待拍照完成
			WaitingForScanDone(60000);
		}

		//进入处理数据状态
		_ScanInfo = ScanInfo(ScanStatus::Process);

		//等待Basecall处理完成
		WaitingForReadyToAcquire(120000);

		//结束Cycle循环
		CPP_LOG_INFO("End cycle");
		if (!ice_EndCycle())
		{
			throw std::exception("End cycle fail");
		}

		//扫描完成
		_ScanInfo = ScanInfo(ScanStatus::Complete);
	}
	catch (const std::exception& ex)
	{
		AbortCycle();
		_ScanError = ex.what();
		CPP_LOG_ERROR(ex.what());
		if (GetScanInfo().status == ScanStatus::Scan)
		{
			_ScanInfo = ScanInfo(ScanStatus::Fail, iCurRow, GetDownloadComplete());
		}
		else
		{
			_ScanInfo = ScanInfo(ScanStatus::Fail);
		}
	}
	catch (...)
	{
		AbortCycle();
		_ScanError = "Unknown Exception";
		CPP_LOG_ERROR("Unknown Exception");
		if (GetScanInfo().status == ScanStatus::Scan)
		{
			_ScanInfo = ScanInfo(ScanStatus::Fail, iCurRow, GetDownloadComplete());
		}
		else
		{
			_ScanInfo = ScanInfo(ScanStatus::Fail);
		}
	}

	//平台运动到装载位置
	_stage->GoToLoadingPos();
}

void ChangGuangClient::WriteFastq()
{
	LaneParamsPtr laneInfo = new LaneParams();
	laneInfo->barcodeLength = _cycleInfo->barcodeLength;// m_barcode3Length;
	laneInfo->barcodePos = _cycleInfo->barcodePos;// m_BarcodePos;
	laneInfo->CalFileFlag = true;
	laneInfo->endCycleProcessMode = R1R2None;// m_EndCycPro;
	laneInfo->filterMode = 0;
	laneInfo->fov2Process.clear();
	laneInfo->fovMaxC = 41;// nCols;
	laneInfo->fovMaxR = 41;// nRows;
	laneInfo->FqPath.clear();
	laneInfo->lane = _cycleInfo->lane;// m_lane;

	laneInfo->read1Length = _cycleInfo->read1Length;// m_read1Length;
	laneInfo->read2Length = _cycleInfo->read2Length;// m_read2Length;
	laneInfo->sequencerType = 80;
	laneInfo->slide = _cycleInfo->slideId;//m_slide;
	laneInfo->speciesBarcodes.clear();
	laneInfo->SpeciesMismatch = 2;//m_barcodeSplitMismatch;
	laneInfo->TotalCycle = _cycleInfo->read1Length + _cycleInfo->read2Length + _cycleInfo->barcodeLength;//nCycs;
	laneInfo->useDefaultSettings = 0;

	laneInfo->WriteFastqMode = 0;

	laneInfo->fov2Process.clear();
	FOVLocation temp_fovL;
	for (int c = 0; c < _cycleInfo->colNums; c++)
	{
		for (int r = 0; r < _cycleInfo->rowNums; r++)
		{
			temp_fovL.col = c + 1;
			temp_fovL.row = r + 1;
			laneInfo->fov2Process.insert(::std::make_pair(temp_fovL, 1));
		}
	}

	/*add sync info*/
	SyncInfo l_SyncInfo;
	l_SyncInfo.mode = NotSync;
	laneInfo->syncInfo = l_SyncInfo;

	CPP_LOG_INFO("generateFQ...");
	ice_generateFQ(laneInfo);


	BatchId b;
	b.slide = _cycleInfo->slideId;
	b.lane = _cycleInfo->lane;
	b.cycle = laneInfo->TotalCycle;
	outFilesDict files;
	ice_checkFQFinished(b, files);
	if (files.size() > 0)
		CPP_LOG_INFO("output fq.gz number: %d, file1 size() = %d", files.size(), files.begin()->second.size());
	else
		CPP_LOG_INFO("no files return");
}
