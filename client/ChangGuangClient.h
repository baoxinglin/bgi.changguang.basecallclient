#pragma once

#include <atomic>
#include <thread>
#include <iostream>
#include "BasecallClient.h"
#include "CgStageInterface.h"

#define MAX_SAVE_IMG_CNT 256

class ChangGuangClient : public BasecallClient
{
public:
	ChangGuangClient(CgStageInterfacePtr stage);
	virtual ~ChangGuangClient();
	void SetSaveImgCol(unsigned int col, bool newVal = true);
	void SetSaveImgRow(unsigned int row, bool newVal = true);
	bool GetSaveImgCol(unsigned int col);
	bool GetSaveImgRow(unsigned int row);
	void WriteFastq();

protected:
	virtual void ScanFlow() override;

private:
	bool _SaveImgCols[MAX_SAVE_IMG_CNT] = { false };
	bool _SaveImgRows[MAX_SAVE_IMG_CNT] = { false };
	CgStageInterfacePtr _stage = nullptr;
};
