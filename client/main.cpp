#include <iostream>
#include "cpplogger.h"
#include "CgStageInterface.h"
#include "ChangGuangClient.h"

using namespace std;

//普通相机接口模拟测试
void SimulateNormalCamera()
{

}

//长光硬件模拟测试
void SimulateChangGuangCamera()
{
	class SimuChangGuangHW : public CgStageInterface
	{
	public:
		// Inherited via CgStageInterface
		virtual void Start(int TriggerNum) override
		{
			_state = CgStageState::Running;
			CPP_LOG_INFO("===>Stage Start()");
		}

		virtual void Start(int ColIdx, int StRowIdx, int EdRowIdx) override
		{
			_state = CgStageState::Running;
			CPP_LOG_INFO("===>Stage Start()");

			Sleep(3000);
			
			if (_cb != nullptr)
			{
				_cb(ColIdx);
			}
		}

		void Stop()
		{
			_state = CgStageState::Idel;
			CPP_LOG_INFO("===>Stage Stop()");
		}

		void GoHome()
		{
			CPP_LOG_INFO("===>Stage GoHome()");
		}

		void GoToLoadingPos()
		{
			CPP_LOG_INFO("===>Stage GoToLoadingPos()");
		}

		CgStageState CheckState()
		{
			return _state;
		}

		void SetScanDoneCallBack(ScanDoneCallBack cb)
		{
			_cb = cb;
		}

	private:
		CgStageState _state = CgStageState::Idel;
		ScanDoneCallBack _cb = nullptr;
	};

	SimuChangGuangHW Stage;
	ChangGuangClient client(&Stage);
	CameraSettings settings = client.GetCameraSettings();
	for (auto i = 0; i < settings.size(); i++)
	{
		settings.at(i).cameraMode = CameraMode::SoftwareTriggerCamera;
	}
	client.SetCameraSettings(settings);
	client.SetSaveImgCol(0);
	client.SetSaveImgRow(0);
	client.SetSaveImgRow(2);
	client.SetSaveImgRow(4);

	//连接basecall，连接成功后自动打开相机，因此必须在之前就设置CamInfo和CamSetting
	//连接basecall的过程较长，basecall需要分配内存和设置相关的运行环境
	cout << "Waiting for connect to basecall..." << endl << endl;
	if (client.Initialize())
	{
		while (true)
		{
			cout << endl << endl;

			int nCycle = 0;
			int nCols = 0;
			int nRows = 0;
			string slideId;
			cout << "Input slideId:";
			cin >> slideId;
			cout << "Input Cycle Number:";
			cin >> nCycle;
			cout << "Input Cols:";
			cin >> nCols;
			cout << "Input Rows:";
			cin >> nRows;

			//设置Cycle信息
			//CycleInfo cycInfo;
			//memset(&cycInfo, 0, sizeof(CycleInfo));
			CycleInfoPtr cycInfo = new CycleInfo();
			cycInfo->slideId = slideId;
			cycInfo->lane = 1;
			cycInfo->cycle = nCycle;
			cycInfo->rowNums = nRows;
			cycInfo->colNums = nCols;
			cycInfo->scanType = "true";
			cycInfo->imageBitPerPixel = 16;
			cycInfo->fovTimeoutMs = 10000;
			cycInfo->read1Length = 5;
			cycInfo->read2Length = 0;
			cycInfo->barcodeLength = 0;
			cycInfo->barcodePos = BarcodePosition::BarcodePosEnd;
			cycInfo->calculateDC = false;
			cycInfo->saveThumbnail = false;
			cycInfo->simulateCameraPath = "E:/TestImages/V2Images/";

			try
			{
				//开始Cycle拍照
				client.StartCycle(cycInfo);

				//等待Cycle拍照结束
				client.WaitingForCycleEnd(30000);
			}
			catch (std::exception& e)
			{
				CPP_LOG_ERROR(e.what());
			}
			catch (...)
			{
				CPP_LOG_ERROR("Unknown Exception");
			}
		}
	}
}

int main(int argc, char* argv[])
{
	//SimulateNormalCamera();

	SimulateChangGuangCamera();
	
	cout << "Press Anykey To Exit !" << endl;
	getchar();

	return 0;
}
