#pragma once

enum CgStageState
{
	Idel = 0,
	Running = 1
};

typedef void(*ScanDoneCallBack)(int index);

class CgStageInterface
{
public:
	virtual void Start(int TriggerNum) = 0;
	virtual void Start(int ColIdx, int StRowIdx, int EdRowIdx) = 0;
	virtual void Stop() = 0;
	virtual void GoHome() = 0;
	virtual void GoToLoadingPos() = 0;
	virtual CgStageState CheckState() = 0;
	virtual void SetScanDoneCallBack(ScanDoneCallBack cb) = 0;
};

typedef CgStageInterface* CgStageInterfacePtr;
